package company.executor;


import company.model.Dummy;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.CountDownLatch;

public class AvgBalanceTaskExecutor extends Thread{

    List<Dummy> dummyList;
    String numOfThread;
    int fromIndex=0, toIndex=0;
    CountDownLatch countDownLatch;

    public AvgBalanceTaskExecutor(int fromIndex, int toIndex, CountDownLatch countDownLatch, List<Dummy> dummyList, String numOfThread){
        this.numOfThread = numOfThread;
        this.dummyList = dummyList;
        this.countDownLatch = countDownLatch;
        this.fromIndex = fromIndex;
        this.toIndex = toIndex;
    }

    @Override
    public void run() {
        System.out.println("start thread " + numOfThread);
        for(int i=fromIndex; i<toIndex; i++){
            Dummy data = dummyList.get(i);
            if(!data.isCheckedBalance()){
                BigDecimal avgBalance = (data.getBalance().add(data.getPrevBalance())).divide(BigDecimal.valueOf(2));
                data.setNewAvgBalance(avgBalance);
                data.setCheckedBalance(true);
                data.setNo1Thread(numOfThread);
            }
        }

        System.out.println("finish thread " + numOfThread);

        countDownLatch.countDown();

    }

}
