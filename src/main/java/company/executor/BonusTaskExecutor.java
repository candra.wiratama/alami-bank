package company.executor;


import company.model.Dummy;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.CountDownLatch;

public class BonusTaskExecutor extends Thread{

    List<Dummy> dummyList;
    String numOfThread;
    CountDownLatch countDownLatch;

    public BonusTaskExecutor(CountDownLatch countDownLatch, List<Dummy> dummyList, String numOfThread){
        this.numOfThread = numOfThread;
        this.dummyList = dummyList;
        this.countDownLatch = countDownLatch;
    }

    @Override
    public void run() {
        System.out.println("start thread-" + numOfThread);
        for(int i=0; i<100; i++){
            Dummy data = dummyList.get(i);
            if(!data.isCheckedBonus()){
                data.setNewBalance(data.getNewBalance().add(new BigDecimal(10)));
                data.setNo3Thread(numOfThread);
                data.setCheckedBonus(true);
            }
        }

        System.out.println("finish thread-" + numOfThread);

        countDownLatch.countDown();

    }

}
