package company.executor;


import company.model.Dummy;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.CountDownLatch;

public class BenefitTaskExecutor extends Thread{

    List<Dummy> dummyList;
    String numOfThread;
    int fromIndex=0, toIndex=0;
    CountDownLatch countDownLatch;

    public BenefitTaskExecutor(int fromIndex, int toIndex, CountDownLatch countDownLatch, List<Dummy> dummyList, String numOfThread){
        this.numOfThread = numOfThread;
        this.dummyList = dummyList;
        this.countDownLatch = countDownLatch;
        this.fromIndex = fromIndex;
        this.toIndex = toIndex;
    }

    @Override
    public void run() {
        System.out.println("start thread No2xThread-" + numOfThread);
        for(int i=fromIndex; i<toIndex; i++){
            Dummy data = dummyList.get(i);
            if(!data.isCheckedBenefit()){
                if(data.getNewBalance().compareTo(new BigDecimal(100)) > 0 && data.getNewBalance().compareTo(new BigDecimal(150)) < 0){
                    data.setNewFreeTf(5);
                    data.setNo2aThread("No 2a Thread-"+numOfThread);
                }
                else if(data.getNewBalance().compareTo(new BigDecimal(150)) > 0){
                    data.setNewBalance(data.getNewBalance().add(new BigDecimal(25)));
                    data.setNo2bThread("No 2b Thread-"+numOfThread);
                }
                data.setCheckedBenefit(true);
            }
        }

        System.out.println("finish thread No2xThread-" + numOfThread);

        countDownLatch.countDown();

    }

}
