package company.builder;


import company.model.Dummy;

import java.math.BigDecimal;
import java.util.ArrayList;

public class DataBuilder {
   static ArrayList<Dummy> dataEod = new ArrayList<>();

    public static void storeDataEod(int id, String name, int age, BigDecimal balance, BigDecimal newBalance, BigDecimal prevBalance, BigDecimal avgBalance,
                      int freeTf){
        Dummy data = new Dummy(id, name, age, balance, newBalance, prevBalance, avgBalance, freeTf, freeTf);
        dataEod.add(data);
    }

    public static ArrayList<Dummy> getDataEod() {
        return dataEod;
    }

    public static void setDataEod(ArrayList<Dummy> dataEod) {
        DataBuilder.dataEod = dataEod;
    }
}
