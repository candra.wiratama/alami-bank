package company.main;

import company.builder.DataBuilder;
import company.executor.AvgBalanceTaskExecutor;
import company.executor.BenefitTaskExecutor;
import company.executor.BonusTaskExecutor;
import company.model.Dummy;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.math.BigDecimal;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CountDownLatch;

public class Demo {
    static List<Dummy> dummyArrayList;

    static{
        try {
            loadFromCsv();
            dummyArrayList = DataBuilder.getDataEod();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws Exception {

        final int NUM_JOBS = 18;
        final CountDownLatch countDownLatch = new CountDownLatch(NUM_JOBS);

        int fromIndex=0, toIndex=40;
        int idxThread = 1;
        for(int i=1; i<=5; i++){
            AvgBalanceTaskExecutor avgBalanceTaskExecutor = new AvgBalanceTaskExecutor(fromIndex, toIndex, countDownLatch, dummyArrayList, "no1Thread-" + idxThread++);
            avgBalanceTaskExecutor.start();
            fromIndex += 40;
            toIndex += 40;
        }

        fromIndex=0; toIndex=40; idxThread=1;

        for(int i=1; i<=5; i++){
            BenefitTaskExecutor benefitTaskExecutor = new BenefitTaskExecutor(fromIndex, toIndex, countDownLatch, dummyArrayList, String.valueOf(idxThread++));
            benefitTaskExecutor.start();
            fromIndex += 40;
            toIndex += 40;
        }

       idxThread=1;

        for(int i=1; i<=8; i++){
            BonusTaskExecutor bonusTaskExecutor = new BonusTaskExecutor(countDownLatch, dummyArrayList, String.valueOf("no3Thread-"+idxThread++));
            bonusTaskExecutor.start();
        }

        countDownLatch.await();
        System.out.println("All workers done. workData=");

        for(Dummy dataDummy : dummyArrayList){
            System.out.println(dataDummy);
        }

        storeToCsv();

    }

    private static void storeToCsv() throws Exception {
//        file:/AlamiBank/out/production/AlamiBank/After%20Eod.csv
        URL res = Demo.class.getClassLoader().getResource("After Eod.csv");
        File file = Paths.get(res.toURI()).toFile();
        FileWriter writer = new FileWriter(file);
        List<String> headers = new ArrayList<>();
        Collections.addAll(headers, "id", "Nama", "Age", "Balanced", "No 2b Thread-No", "No 3 Thread-No", "Previous Balanced", "Average Balanced", "No 1 Thread-No", "Free Transfer", "No 2a Thread-No");
        writer.append(String.join(";", headers));
        writer.append("\n");

        try {
            for (Dummy data : dummyArrayList) {
                writer.append(String.join(";", data.toArray()));
                writer.append("\n");
            }
            writer.close();

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private static void loadFromCsv() throws Exception{
//        file:/AlamiBank/out/production/AlamiBank/Before%20Eod.csv
        URL res = Demo.class.getClassLoader().getResource("Before Eod.csv");
        File file = Paths.get(res.toURI()).toFile();
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            int idx = 0;
            while ((line = br.readLine()) != null) {
                if(idx > 0){
                    String[] values = line.split(";");
                    int id = Integer.parseInt(values[0]);
                    String name = values[1];
                    int age = Integer.parseInt(values[2]);
                    BigDecimal balance = new BigDecimal(Integer.parseInt(values[3]));
                    BigDecimal prevBalance = new BigDecimal(Integer.parseInt(values[4]));
                    BigDecimal avgBalance = new BigDecimal(Integer.parseInt(values[5]));
                    int freeTf = Integer.parseInt(values[6]);

                    DataBuilder.storeDataEod(id, name, age, balance, balance, prevBalance, avgBalance, freeTf);
                }
                idx++;
            }
        }
    }
}