package company.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;

public class Dummy {
    private int id;
    private String name;
    private int age;
    private BigDecimal balance;
    private BigDecimal newBalance;
    private BigDecimal prevBalance;
    private BigDecimal avgBalance;
    private BigDecimal newAvgBalance;
    private int freeTf;
    private int newFreeTf;
    private String no1Thread="";
    private String no2aThread="";
    private String no2bThread="";
    private String no3Thread="";
    private boolean checkedBalance = false;
    private boolean checkedBenefit = false;
    private boolean checkedBonus = false;

    public String[] toArray(){
        ArrayList<String> data = new ArrayList<>();

        Collections.addAll(data, String.valueOf(id), name, String.valueOf(age), newBalance.toString(), no2bThread, no3Thread, prevBalance.toString(),
                avgBalance.toString(), no1Thread, String.valueOf(newFreeTf), no2aThread);
        Object dataArr [] = data.toArray();

        String[] dest = new String[dataArr.length];
        System.arraycopy(dataArr, 0, dest, 0, dataArr.length);
        return dest;
    }

    public boolean isCheckedBonus() {
        return checkedBonus;
    }

    public void setCheckedBonus(boolean checkedBonus) {
        this.checkedBonus = checkedBonus;
    }

    public String getNo1Thread() {
        return no1Thread;
    }

    public void setNo1Thread(String no1Thread) {
        this.no1Thread = no1Thread;
    }

    public String getNo2aThread() {
        return no2aThread;
    }

    public void setNo2aThread(String no2aThread) {
        this.no2aThread = no2aThread;
    }

    public String getNo2bThread() {
        return no2bThread;
    }

    public void setNo2bThread(String no2bThread) {
        this.no2bThread = no2bThread;
    }

    public String getNo3Thread() {
        return no3Thread;
    }

    public void setNo3Thread(String no3Thread) {
        this.no3Thread = no3Thread;
    }

    public boolean isCheckedBenefit() {
        return checkedBenefit;
    }

    public void setCheckedBenefit(boolean checkedBenefit) {
        this.checkedBenefit = checkedBenefit;
    }

    public boolean isCheckedBalance() {
        return checkedBalance;
    }

    public void setCheckedBalance(boolean checkedBalance) {
        this.checkedBalance = checkedBalance;
    }

    public Dummy(int id, String name, int age, BigDecimal balance, BigDecimal newBalance, BigDecimal prevBalance, BigDecimal avgBalance, int freeTf, int newFreeTf) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.balance = balance;
        this.prevBalance = prevBalance;
        this.avgBalance = avgBalance;
        this.freeTf = freeTf;
        this.newBalance = newBalance;
        this.newFreeTf = newFreeTf;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getPrevBalance() {
        return prevBalance;
    }

    public void setPrevBalance(BigDecimal prevBalance) {
        this.prevBalance = prevBalance;
    }

    public BigDecimal getAvgBalance() {
        return avgBalance;
    }

    public void setAvgBalance(BigDecimal avgBalance) {
        this.avgBalance = avgBalance;
    }

    public int getFreeTf() {
        return freeTf;
    }

    public void setFreeTf(int freeTf) {
        this.freeTf = freeTf;
    }

    public BigDecimal getNewBalance() {
        return newBalance;
    }

    public void setNewBalance(BigDecimal newBalance) {
        this.newBalance = newBalance;
    }

    public BigDecimal getNewAvgBalance() {
        return newAvgBalance;
    }

    public void setNewAvgBalance(BigDecimal newAvgBalance) {
        this.newAvgBalance = newAvgBalance;
    }

    public int getNewFreeTf() {
        return newFreeTf;
    }

    public void setNewFreeTf(int newFreeTf) {
        this.newFreeTf = newFreeTf;
    }

    @Override
    public String toString() {
        return "BeforeEod{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", balance=" + balance +
                ", newBalance=" + newBalance +
                ", prevBalance=" + prevBalance +
                ", avgBalance=" + avgBalance +
                ", newAvgBalance=" + newAvgBalance +
                ", freeTf=" + freeTf +
                ", newFreeTf=" + newFreeTf +
                ", no1Thread='" + no1Thread + '\'' +
                ", no2aThread='" + no2aThread + '\'' +
                ", no2bThread='" + no2bThread + '\'' +
                ", no3Thread='" + no3Thread + '\'' +
                ", checkedBalance=" + checkedBalance +
                ", checkedBenefit=" + checkedBenefit +
                ", checkedBonus=" + checkedBonus +
                '}';
    }
}
